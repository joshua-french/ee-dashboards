<?php

/*    $stopwords = array("a", "about", "i", "above", "above", "", "-", "across", "after", "!","afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although","always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be","became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom","but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the");*/


    $keepwords = array("app","website","phone","sim","data","text","texts","chat","service","upgrade","navigation","navigate","gallery","homepage","price","discount","banner","deal","deals","banners","basket","purchase","buy","4g","3g","network","signal","samsung","iphone","email","sms","design","wifi","checkout","error","150","contract","contact","broadband","p10","s8","nokia","coverage","login","payg","tablet","tv","bt","datafest","unlimited","ee","slow","great","terrible","annoying","confusing","information","simple","details","pay","account","myee","info","agent","roaming","register","orange","t-mobile","bill","bills","add-ons","insurance","pixel","iphone7","android","business","store","value","login","problem","issue");

    $keepphrases = array("sim only", "contract phone","pay monthly","apple music","bt sport","top up","pay monthly","topped up","log in","live chat","wifi calling", "4g coverage", "network problem", "no signal","contract phone","new customer","upgrade phone","login problem","password reset","pay as you go");

    header("Access-Control-Allow-Origin: *");

    // date forat is 2017-05-24
    $startdate = $_POST["today"];
    $enddate = $_POST["tomorrow"];

    
/*    $startdate = "2017-06-09";
    $enddate = "2017-06-10";*/
  //  echo json_encode("start data is ".$startdate." and end date is ".$enddate);
    
    $char_limit = 300;
    $type= "group";
    $xml_styles = "2";
    //$realm = "36849";
    $realm = "27791"; //27791 - all

    $query = "?&start_date=".$startdate."&end_date=".$enddate."&type=".$type."&realm=".$realm."&xml_style=".$xml_styles;

    // this is my XML call
    //$feed = simplexml_load_file('https://eeuk:DataInsight123!@webservice.opinionlab.com/display/'.$query);

    $feed = simplexml_load_file('https://eeuk:Testing123!@webservice.opinionlab.com/display/'.$query);

   // $feed = simplexml_load_file('test.xml');

  //  $feed = simplexml_load_file('http://mrliger.com/test/bad-data-1.june.9.46.am.xml');

// hard coded feed https://eeuk:Insight123!@webservice.opinionlab.com/display/?&start_date=2017-05-29&end_date=2017-05-30&type=group&realm=36849&xml_style=2

    $Json = json_encode($feed);

    // defining a blank array where I want the result to go
    $mappedWordsObj = [];
    $removedLargeComments = [];

//new forloop
    for($i = 0;$i < count($feed->data);$i++) {
        
        $strng_length = mb_strlen($feed->data[$i]->comments);
        
       if($strng_length < $char_limit) {
        
            $arr_1 = [];
            // if I echo this it returns me a string with no quotes (sometimes the quotes would be in there and I thought this may have been the issue)
            $comment = chop(strtolower($feed->data[$i]->comments));
           
            $cleanPhrases = str_replace(array('.', ',','!','?'), '' , $comment);

            //go over key phrases array
            for($j = 0;$j < count($keepphrases); $j++) {

                if (strpos($cleanPhrases, $keepphrases[$j]) !== false) {
                    array_push($arr_1,$keepphrases[$j]);
                }

            };            

            //remove full stop and commas etc
            $cleanWords = str_replace(array('.', ',','!','?'), '' , $comment);

            // here I split the comment into an array by spaces 
            $wordsArray = explode(' ', $cleanWords);

             //go over keywords array
            for($j = 0;$j < count($wordsArray); $j++) {
                for($k = 0;$k < count($keepwords); $k++) {
                    if($wordsArray[$j] === $keepwords[$k]) {
                        array_push($arr_1, $keepwords[$k]);
                    }
                }
            }
            
        

            array_push($mappedWordsObj, array_values($arr_1));
            
            array_push($removedLargeComments, $feed->data[$i]);
            
       }    
      
   };

   // $dates = "start data is ".$startdate." and end date is ".$enddate;

    $mappedData = [json_encode($removedLargeComments),$mappedWordsObj];

    //$mappedData = [json_encode($Json),$mappedWordsObj];

    // here I push to the DOM the result to see how my array looks
   echo json_encode($mappedData);

?>